﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace CondoAdmin.Models
{
    public class Property
    {
        public int id { get; set; }
        public int number { get; set; }
        public int apto { get; set; }

        public int customerId { get; set; }

        [ForeignKey("customerId")]        
        public Customer customer { get; set; }

        public Property()
        {
            
        }
    }
}
 