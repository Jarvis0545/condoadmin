﻿using System;
using Microsoft.EntityFrameworkCore;

namespace CondoAdmin.Models
{
    public class CondoAdminDbContext : DbContext
    {
        
        public DbSet<Property> properties { get; set; }
        public DbSet<Customer> customers { get; set; }
        public DbSet<Payment> payments { get; set; }

        public CondoAdminDbContext( DbContextOptions<CondoAdminDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=localhost;Database=condoadmin;User Id=sa;Password=Admin123@;");
        }
    }
}




//// GET: api/TodoItems/5
//[HttpGet("{id}")]
//public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
//{
//    var todoItem = await _context.TodoItems.FindAsync(id);

//    if (todoItem == null)
//    {
//        return NotFound();
//    }

//    return todoItem;
//}

