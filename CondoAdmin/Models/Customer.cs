﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CondoAdmin.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string? email { get; set; }

        //[DefaultValue(true)]
        public bool active { get; set; } = true;

        public List<Property> property { get; set; }
        public List<Payment> payments { get; set; }

        public Customer()
        {
        }
    }
}
