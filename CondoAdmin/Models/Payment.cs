﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CondoAdmin.Models
{
    public class Payment
    {
        public int id { get; set; }
        public decimal amount { get; set; }
        public bool payment { get; set; }
        public DateTime generated { get; set; }
        public DateTime? payDay { get; set; }

        public int customerId { get; set; }

        [ForeignKey("customerId")]
        public Customer customer { get; set; }

        public Payment()
        {
        }
    }
}
