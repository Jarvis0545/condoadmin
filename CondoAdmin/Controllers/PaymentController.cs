﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CondoAdmin.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CondoAdmin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : Controller
    {
        private CondoAdminDbContext context;

        public PaymentController(CondoAdminDbContext ca)
        {
            context = ca;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Payment>>> GetPayments()
        {
            return await context.payments.Include(c => c.customer).ThenInclude(p => p.property)
                .ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Payment>>> GetPaymentByCustomer(int id)
        {
            var payments = await context.payments.Where(p => p.customerId == id).ToListAsync();

            if( payments == null)
            {
                return NotFound();
            }

            return payments;
        }
        //[Route("/Payment/GetPayment")]
        [HttpGet("/Payment/GetPayment/{id}")]
        public async Task<ActionResult<Payment>> GetPaymentById(int id)
        {
            var payment = await context.payments.Where(p => p.id == id).FirstOrDefaultAsync();
            if(payment == null)
            {
                return NotFound();
            }

            return payment;
        }

        [HttpPost]
        public async Task<ActionResult<Payment>> CreatePayment(Payment payment)
        {
            context.payments.Add(payment);
            await context.SaveChangesAsync();

            return await context.payments.FindAsync(payment.id);
        }

        [HttpPut]
        public async Task<ActionResult<Payment>> PayPayment(Payment payment)
        {
            var p = await context.payments.FindAsync(payment.id);

            p.payment = true;

            await context.SaveChangesAsync();

            return await GetPaymentById(payment.id);
        }

    }
}
