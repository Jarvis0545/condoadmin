﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CondoAdmin.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CondoAdmin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController: Controller
    {
        private CondoAdminDbContext context;

        public CustomerController(CondoAdminDbContext ca) 
        {
            context = ca;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomers()
        {
            return await context.customers.Include(p => p.property).ToListAsync();
            
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(int id)
        {
            var c = await context.customers.FindAsync(id);
            if(c == null)
            {
                return NotFound();
            }

            return c;
        }

        [HttpPost]
        public async Task<ActionResult<Customer>> PostCustomer(Customer customer)
        {
            context.customers.Add(customer);
            await context.SaveChangesAsync();

            return await GetCustomer(customer.id);
        }
        [HttpPut]
        public async Task<ActionResult<Customer>> UpdateCustomer(Customer customer)
        {
            //var c = await context.customers.FindAsync(customer.id);
            //if ( c == null)
            //{
            //    return NotFound();
            //}

            context.Update<Customer>(customer);
            await context.SaveChangesAsync();

            return await GetCustomer(customer.id);

        }

    }
}
