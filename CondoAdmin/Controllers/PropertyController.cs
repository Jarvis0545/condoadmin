﻿using System;
using Microsoft.AspNetCore.Mvc;
using CondoAdmin.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CondoAdmin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PropertyController : Controller
    {
        private CondoAdminDbContext context;

        public PropertyController(CondoAdminDbContext ca)
        {
            context = ca;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Property>>> GetProperties()
        {
            return await context.properties.Include(c => c.customer).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Property>> GetProperty(int id)
        {
            var p = await context.properties.FindAsync(id);

            return p;
        }

        [HttpPost]
        public async Task<ActionResult<Property>> PostProperty(Property property)
        {
            context.properties.Add(property);
            await context.SaveChangesAsync();

            return await GetProperty(property.id);
        }
        [HttpPut]
        public async Task<ActionResult<Property>> UpdateCustomer(Property property)
        {
            context.Update<Property>(property);
            await context.SaveChangesAsync();

            return await GetProperty(property.id);

        }

    }
}
