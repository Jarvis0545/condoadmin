﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CondoAdmin.Migrations
{
    public partial class customerEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "customers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "email",
                table: "customers");
        }
    }
}
