﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CondoAdmin.Migrations
{
    public partial class AddingActiveCustomer2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "active",
                table: "properties",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active",
                table: "customers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "active",
                table: "properties");

            migrationBuilder.DropColumn(
                name: "active",
                table: "customers");
        }
    }
}
