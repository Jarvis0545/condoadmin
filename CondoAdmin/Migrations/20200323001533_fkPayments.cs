﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CondoAdmin.Migrations
{
    public partial class fkPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "pay",
                table: "payments");

            migrationBuilder.AddColumn<DateTime>(
                name: "payDay",
                table: "payments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "payDay",
                table: "payments");

            migrationBuilder.AddColumn<DateTime>(
                name: "pay",
                table: "payments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
