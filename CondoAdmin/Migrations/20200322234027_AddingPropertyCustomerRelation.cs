﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CondoAdmin.Migrations
{
    public partial class AddingPropertyCustomerRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "active",
                table: "properties");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "active",
                table: "properties",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
